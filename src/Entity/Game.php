<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\GameRepository")
 */
class Game
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer")
     */
    private $tour;

    /**
     * @ORM\Column(type="datetime")
     */
    private $date_math;

    /**
     * @ORM\Column(type="integer")
     */
    private $playField;

    /**
     * @var Command $homeCommand
     * @ORM\ManyToOne(targetEntity="App\Entity\Command", inversedBy="matchOnHome")
     */
    private $homeCommand;

    /**
     * @var Command $guestCommand
     * @ORM\ManyToOne(targetEntity="App\Entity\Command", inversedBy="matchOnQuest")
     */
    private $guestCommand;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $homeCommandGoal;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $guestCommandGoal;

    /**
     * @ORM\Column(type="boolean")
     */
    private $status;


    public function getId():? int
    {
        return $this->id;
    }

    /**
     * @param mixed $tour
     * @return Game
     */
    public function setTour($tour)
    {
        $this->tour = $tour;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getTour()
    {
        return $this->tour;
    }

    /**
     * @param mixed $date_math
     * @return Game
     */
    public function setDateMath($date_math)
    {
        $this->date_math = $date_math;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getDateMath()
    {
        return $this->date_math;
    }

    /**
     * @param mixed $playField
     * @return Game
     */
    public function setPlayField($playField)
    {
        $this->playField = $playField;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getPlayField()
    {
        return $this->playField;
    }

    /**
     * @param Command $homeCommand
     * @return Game
     */
    public function setHomeCommand(Command $homeCommand): ?Game
    {
        $this->homeCommand = $homeCommand;
        return $this;
    }

    /**
     * @return Command
     */
    public function getHomeCommand(): ?Command
    {
        return $this->homeCommand;
    }

    /**
     * @param Command $guestCommand
     * @return Game
     */
    public function setGuestCommand(Command $guestCommand): Game
    {
        $this->guestCommand = $guestCommand;
        return $this;
    }

    /**
     * @return Command
     */
    public function getGuestCommand(): ?Command
    {
        return $this->guestCommand;
    }

    /**
     * @param mixed $homeCommandGoal
     * @return Game
     */
    public function setHomeCommandGoal($homeCommandGoal)
    {
        $this->homeCommandGoal = $homeCommandGoal;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getHomeCommandGoal()
    {
        return $this->homeCommandGoal;
    }

    /**
     * @param mixed $guestCommandGoal
     * @return Game
     */
    public function setGuestCommandGoal($guestCommandGoal)
    {
        $this->guestCommandGoal = $guestCommandGoal;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getGuestCommandGoal()
    {
        return $this->guestCommandGoal;
    }

    /**
     * @param mixed $status
     * @return Game
     */
    public function setStatus($status)
    {
        $this->status = $status;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getStatus()
    {
        return $this->status;
    }
}
