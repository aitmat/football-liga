<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\CommandRepository")
 */
class Command
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @var Division $division
     * @ORM\ManyToOne(targetEntity="App\Entity\Division", inversedBy="command")
     */
    private $division;

    /**
     * @var Player $player
     * @ORM\OneToMany(targetEntity="App\Entity\Player", mappedBy="command")
     */
    private $player;

    /**
     * @var Game $matchOnQuest
     * @ORM\OneToMany(targetEntity="App\Entity\Game", mappedBy="guestCommand")
     */
    private $matchOnQuest;

    /**
     * @var Game $matchOnQuest
     * @ORM\OneToMany(targetEntity="App\Entity\Game", mappedBy="homeCommand")
     */
    private $matchOnHome;

    /**
     * @var ResultGame $points
     * @ORM\OneToMany(targetEntity="ResultGame", mappedBy="command")
     */
    private $points;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @param Division $division
     * @return Command
     */
    public function setDivision(Division $division): Command
    {
        $this->division = $division;
        return $this;
    }

    /**
     * @return Division
     */
    public function getDivision():? Division
    {
        return $this->division;
    }

    /**
     * @param Player $player
     * @return Command
     */
    public function setPlayer(Player $player): Command
    {
        $this->player = $player;
        return $this;
    }

    /**
     * @return Player
     */
    public function getPlayer(): Player
    {
        return $this->player;
    }

    /**
     * @param Game $matchOnQuest
     * @return Command
     */
    public function setMatchOnQuest(Game $matchOnQuest): Command
    {
        $this->matchOnQuest = $matchOnQuest;
        return $this;
    }

    /**
     * @param Game $matchOnHome
     * @return Command
     */
    public function setMatchOnHome(Game $matchOnHome): Command
    {
        $this->matchOnHome = $matchOnHome;
        return $this;
    }

    /**
     * @return Game
     */
    public function getMatchOnHome(): Game
    {
        return $this->matchOnHome;
    }

    /**
     * @param ResultGame $points
     * @return Command
     */
    public function setPoints(ResultGame $points): Command
    {
        $this->points = $points;
        return $this;
    }

    /**
     * @return ResultGame
     */
    public function getPoints(): ResultGame
    {
        return $this->points;
    }
}
