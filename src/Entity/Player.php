<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\File;
use Vich\UploaderBundle\Mapping\Annotation as Vich;

/**
 * @ORM\Entity(repositoryClass="App\Repository\PlayerRepository")
 * @Vich\Uploadable
 */
class Player
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $surname;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $avatarName;

    /**
     * @Vich\UploadableField(mapping="player_avatar", fileNameProperty="avatarName")
     * @var File
     */
    private $avatarFile;

    /**
     * @var Command $command
     * @ORM\ManyToOne(targetEntity="App\Entity\Command", inversedBy="player")
     */
    private $command;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @param mixed $surname
     * @return Player
     */
    public function setSurname($surname)
    {
        $this->surname = $surname;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getSurname()
    {
        return $this->surname;
    }

    /**
     * @param Command $command
     * @return Player
     */
    public function setCommand(Command $command): Player
    {
        $this->command = $command;
        return $this;
    }

    /**
     * @return Command
     */
    public function getCommand():? Command
    {
        return $this->command;
    }

    /**
     * @param mixed $avatarName
     * @return Player
     */
    public function setAvatarName($avatarName)
    {
        $this->avatarName = $avatarName;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getAvatarName()
    {
        return $this->avatarName;
    }

    /**
     *
     * @param File|\Symfony\Component\HttpFoundation\File\UploadedFile $image
     *
     * @return void
     */

    public function setAvatarFile(?File $image = null)
    {
        $this->avatarFile = $image;
    }

    /**
     * @return File|null
     */

    public function getAvatarFile()
    {
        return $this->avatarFile;
    }
}
