<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\DivisionRepository")
 */
class Division
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @var Command $command
     * @ORM\OneToMany(targetEntity="App\Entity\Command", mappedBy="division")
     */
    private $command;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @param Command $command
     * @return Division
     */
    public function setCommand(Command $command): Division
    {
        $this->command = $command;
        return $this;
    }

    /**
     * @return Command
     */
    public function getCommand(): Command
    {
        return $this->command;
    }
}
