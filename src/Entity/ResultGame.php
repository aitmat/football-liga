<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ResultGameRepository")
 */
class ResultGame
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @var Command $command
     * @ORM\ManyToOne(targetEntity="App\Entity\Command", inversedBy="points")
     */
    private $command;

    /**
     * @ORM\Column(type="integer", length=2)
     */
    private $point;

    /**
     * @ORM\Column(type="integer", length=2)
     */
    private $goal_scored;

    /**
     * @ORM\Column(type="integer", length=2)
     */
    private $missed_goal;

    /**
     * @ORM\Column(type="integer", length=2)
     */
    private $tour;

    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @param Command $command
     * @return ResultGame
     */
    public function setCommand(Command $command): ResultGame
    {
        $this->command = $command;
        return $this;
    }

    /**
     * @return Command
     */
    public function getCommand(): Command
    {
        return $this->command;
    }

    /**
     * @param mixed $point
     * @return ResultGame
     */
    public function setPoint($point)
    {
        $this->point = $point;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getPoint()
    {
        return $this->point;
    }

    /**
     * @param mixed $tour
     * @return ResultGame
     */
    public function setTour($tour)
    {
        $this->tour = $tour;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getTour()
    {
        return $this->tour;
    }

    /**
     * @param mixed $goal_scored
     * @return ResultGame
     */
    public function setGoalScored($goal_scored)
    {
        $this->goal_scored = $goal_scored;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getGoalScored()
    {
        return $this->goal_scored;
    }

    /**
     * @param mixed $missed_goal
     * @return ResultGame
     */
    public function setMissedGoal($missed_goal)
    {
        $this->missed_goal = $missed_goal;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getMissedGoal()
    {
        return $this->missed_goal;
    }
}
