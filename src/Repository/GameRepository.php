<?php

namespace App\Repository;

use App\Entity\Game;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\NonUniqueResultException;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Game|null find($id, $lockMode = null, $lockVersion = null)
 * @method Game|null findOneBy(array $criteria, array $orderBy = null)
 * @method Game[]    findAll()
 * @method Game[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class GameRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Game::class);
    }

    /**
     * @param $first_command_id
     * @param $second_command_id
     * @param $tour
     * @return Game|null
     */
    public function findByCommandIdAndTour($first_command_id, $second_command_id, $tour)
    {
        try {
            return $this->createQueryBuilder('a')
                ->where('a.homeCommand = :first_command_id')
                ->orWhere('a.homeCommand = :second_command_id')
                ->orWhere('a.guestCommand = :first_command_id')
                ->orWhere('a.guestCommand = :second_command_id')
                ->andWhere('a.tour = :tour')
                ->setParameter('first_command_id', $first_command_id)
                ->setParameter('second_command_id', $second_command_id)
                ->setParameter('tour', $tour)
                ->setMaxResults(1)
                ->getQuery()
                ->getOneOrNullResult();
        } catch (NonUniqueResultException $e) {
            return null;
        }
    }

    /**
     * @return Game[] Returns an array of Game objects
     */
    public function findAllFinishedGames()
    {
        return $this->createQueryBuilder('a')
            ->where('a.status = :true')
            ->setParameter('true', true)
            ->setMaxResults(100)
            ->getQuery()
            ->getResult();
    }

    /**
     * @return Game[] Returns an array of Division objects
     */
    public function findAllFutureGames()
    {
        return $this->createQueryBuilder('a')
            ->where('a.status = :false')
            ->setParameter('false', false)
            ->setMaxResults(100)
            ->getQuery()
            ->getResult();
    }
}
