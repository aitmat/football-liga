<?php

namespace App\Repository;

use App\Entity\Game;
use App\Entity\ResultGame;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\NonUniqueResultException;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method ResultGame|null find($id, $lockMode = null, $lockVersion = null)
 * @method ResultGame|null findOneBy(array $criteria, array $orderBy = null)
 * @method ResultGame[]    findAll()
 * @method ResultGame[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ResultGameRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, ResultGame::class);
    }

    /**
     * @param $command_id
     * @param $tour
     * @return ResultGame|null
     */
    public function findByPointCommand($command_id, $tour)
    {
        try {
            return $this->createQueryBuilder('a')
                ->where('a.command = :command_id')
                ->andWhere('a.tour = :tour')
                ->setParameter('command_id', $command_id)
                ->setParameter('tour', $tour)
                ->setMaxResults(1)
                ->getQuery()
                ->getOneOrNullResult();
        } catch (NonUniqueResultException $e) {
            return null;
        }
    }




    /*
    public function findOneBySomeField($value): ?Division
    {
        return $this->createQueryBuilder('d')
            ->andWhere('d.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
